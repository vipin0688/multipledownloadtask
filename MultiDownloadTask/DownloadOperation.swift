//
//  DownloadOpeartion.swift
//  MultiDownloadTask
//
//  Created by AB69667 on 04/12/19.
//  Copyright © 2019 AB69667. All rights reserved.
//

import UIKit

class DownloadOperationQueue : NSObject{
    lazy var downloadQueue: OperationQueue = {
      var queue = OperationQueue()
      queue.name = "Download queue"
      queue.maxConcurrentOperationCount = 5
      return queue
    }()
}
class DownloadOperation: Operation {
    private var task : URLSessionDownloadTask!
    
    enum OperationState : Int {
        case ready
        case executing
        case finished
    }
    
    // default state is ready (when the operation is created)
    private var state : OperationState = .ready {
        willSet {
            self.willChangeValue(forKey: "isExecuting")
            self.willChangeValue(forKey: "isFinished")
        }
        
        didSet {
            self.didChangeValue(forKey: "isExecuting")
            self.didChangeValue(forKey: "isFinished")
        }
    }
    
    override var isReady: Bool { return state == .ready }
    override var isExecuting: Bool { return state == .executing }
    override var isFinished: Bool { return state == .finished }
    
    init(session: URLSession, downloadTaskURL: URL,downloadTaskInfo:DownloadTaskInfo, completionHandler: ((URL?, URLResponse?, Error?) -> Void)?) {
        super.init()
        
        // use weak self to prevent retain cycle
        task = session.downloadTask(with: downloadTaskURL)
        downloadTaskInfo.downloadTask = task
        downloadTaskInfo.downloadTaskId = downloadTaskInfo.downloadTask?.taskIdentifier
        downloadTaskInfo.isDownload = true;
        downloadTaskInfo.downloadedData = nil;

    }
    
    override func start() {
        /*
         if the operation or queue got cancelled even
         before the operation has started, set the
         operation state to finished and return
         */
        if(self.isCancelled) {
            state = .finished
            return
        }
        
        // set the state to executing
        state = .executing
        
        print("downloading \(self.task.originalRequest?.url?.absoluteString ?? "")")
            
            // start the downloading
            self.task.resume()
    }
    
    override func cancel() {
        super.cancel()
        
        // cancel the downloading
        self.task.cancel()
    }
}
