//
//  DownloadService.swift
//  MultiDownloadTask
//
//  Created by AB69667 on 03/12/19.
//  Copyright © 2019 AB69667. All rights reserved.
//

import UIKit

protocol ProgressBarUpdateDelegate {
    func updateProgressBar(at index: IndexPath)
}
class DownloadService: NSObject,URLSessionDelegate,URLSessionDownloadDelegate {
    
    
    var downloadTaskList : [DownloadTaskInfo] = []
    var session : URLSession!
    let downloadOperationsQueue = DownloadOperationQueue()
    var progressUpdateDelegate : ProgressBarUpdateDelegate!
    override init() {
        super.init()
        self.session = URLSession(configuration: URLSessionConfiguration.default, delegate: self, delegateQueue: downloadOperationsQueue.downloadQueue)

    }
    func startDownloading(urlString:String){
       var downloadInfo = DownloadTaskInfo()
        downloadInfo.url = urlString
        downloadInfo.name = urlString
        self.downloadTaskList.append(downloadInfo)
        let url: URL =  URL(string:downloadInfo.url ?? "")!
        let operation = DownloadOperation(session: session, downloadTaskURL: url,downloadTaskInfo: downloadInfo, completionHandler: { (localURL, urlResponse, error) in
            
            if error == nil {
                DispatchQueue.main.async {
                 
                }
            }
        })
        self.downloadOperationsQueue.downloadQueue.addOperation(operation)
        print("count = ",self.downloadOperationsQueue.downloadQueue.operationCount)
        
    }
    
    func urlSession(_ session: URLSession, downloadTask: URLSessionDownloadTask, didWriteData bytesWritten: Int64, totalBytesWritten: Int64, totalBytesExpectedToWrite: Int64) {

        for (index, downloadTaskInfo) in self.downloadTaskList.enumerated() {
            if URL(string:downloadTaskInfo.url!) == downloadTask.currentRequest?.url{
                if totalBytesExpectedToWrite > 0 {
                    downloadTaskInfo.progress = Float(totalBytesWritten) / Float(totalBytesExpectedToWrite)
                    self.progressUpdateDelegate.updateProgressBar(at: NSIndexPath(row: index, section: 0) as IndexPath)

                }
            }
    }
    }

    func urlSession(_ session: URLSession, downloadTask: URLSessionDownloadTask, didFinishDownloadingTo location: URL) {
        
    }

    func urlSession(_ session: URLSession, task: URLSessionTask, didCompleteWithError error: Error?) {
        debugPrint("Task completed: \(task), error: \(error)")
    }
}
