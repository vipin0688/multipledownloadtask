//
//  DownloadTaskCell.swift
//  MultiDownloadTask
//
//  Created by AB69667 on 03/12/19.
//  Copyright © 2019 AB69667. All rights reserved.
//

import UIKit

class DownloadTaskCell: UITableViewCell {

    @IBOutlet weak var lblUrlName: UILabel!
    
    @IBOutlet weak var progressBar: UIProgressView!
    @IBOutlet weak var lblDownloadPercent: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
