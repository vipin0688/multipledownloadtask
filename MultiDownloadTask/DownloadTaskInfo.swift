//
//  DownloadTaskInfo.swift
//  MultiDownloadTask
//
//  Created by AB69667 on 03/12/19.
//  Copyright © 2019 AB69667. All rights reserved.
//

import UIKit

class DownloadTaskInfo: NSObject {
        var name:String?;
        var url:String?;
        var isDownload:Bool = false;
        var progress:Float = 0.0;
        var downloadTask:URLSessionDownloadTask?;
        var downloadTaskId:Int?;
        var downloadedData:NSData?;

}
