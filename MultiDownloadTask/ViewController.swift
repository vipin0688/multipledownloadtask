//
//  ViewController.swift
//  MultiDownloadTask
//
//  Created by AB69667 on 03/12/19.
//  Copyright © 2019 AB69667. All rights reserved.
//

import UIKit

class DownloadViewController: UIViewController {
    var downloadService:DownloadService!

    @IBOutlet weak var downloadTaskTable: UITableView!
    
    @IBOutlet weak var textFieldUrl: UITextField!
    override func viewDidLoad() {
        super.viewDidLoad()
        textFieldUrl.text = "https://scholar.princeton.edu/sites/default/files/oversize_pdf_test_0.pdf"
        downloadService = DownloadService()
        downloadService.progressUpdateDelegate = self
        // Do any additional setup after loading the view.
    }

    @IBAction func btnActionDownload(_ sender: Any) {
        textFieldUrl.resignFirstResponder()
        downloadService.startDownloading(urlString: textFieldUrl.text!)
        self.downloadTaskTable.reloadData()
    }
    
}

extension DownloadViewController:UITableViewDelegate,UITableViewDataSource,ProgressBarUpdateDelegate,UITextFieldDelegate{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return downloadService.downloadTaskList.count;
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
         var cell = tableView.dequeueReusableCell(withIdentifier: "DownloadTaskCell") as? DownloadTaskCell;
               if(cell == nil) {
                cell = DownloadTaskCell(style: UITableViewCell.CellStyle.default, reuseIdentifier: "DownloadTaskCell");
               }
        let downloadInfo = downloadService.downloadTaskList[indexPath.row] as DownloadTaskInfo
        cell?.lblUrlName.text = downloadInfo.name;
        cell?.progressBar.progress = downloadInfo.progress;
        cell?.lblDownloadPercent.text = String(Int(downloadInfo.progress * 100)) + "%";

        return cell!;
    }
    
    func updateProgressBar(at index: IndexPath){
        DispatchQueue.main.async {
            self.downloadTaskTable.reloadData()
        }
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        downloadService.startDownloading(urlString: textFieldUrl.text!)
        self.downloadTaskTable.reloadData()
        return true
    }

}
